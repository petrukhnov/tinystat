package com.kpetruk.tinystat.test;

import com.kpetruk.tinystat.Statistics;

public class ListTest {

  /**
   * @param args
   */
  public static void main(String[] args) {
    // TODO Auto-generated method stub
  //init
    Statistics s = new Statistics(); //check that default constructor working
    
    //fill
    for (int i = 0; i < 10; i++){
      s.increment("filesProcessed");
      if (i == 3 || i == 7) {
        s.increment("filesFailed");
        s.addToList("filesFailed", "12345"+i+".jpg");
      } else {
        s.increment("filesTransferred");
      }
    }
    
    //out
    System.out.println(s.outAll());
    
    System.out.println(s.outSingle("filesProcessed"));
    System.out.println(s.outSingle("filesFailed"));
    System.out.println(s.outSingle("filesTransferred"));
    System.out.println(s.outSingle("notexistingvalue"));
    
  }

}
