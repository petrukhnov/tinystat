package com.kpetruk.tinystat.test;

import com.kpetruk.tinystat.Statistics;

public class SimpleTest {
  
  public static void main(String[] args) {
    //init
    Statistics s = Statistics.getInstance();
    
    //fill
    for (int i = 0; i < 10; i++){
      s.increment("filesProcessed");
      if (i == 3 || i == 7) {
        s.increment("filesFailed");
      } else {
        s.increment("filesTransferred");
      }
    }
    
    //out
    System.out.println(s.outAll());
    
    System.out.println(s.outSingle("filesProcessed"));
    System.out.println(s.outSingle("filesFailed"));
    System.out.println(s.outSingle("filesTransferred"));
    System.out.println(s.outSingle("notexistingvalue"));
  }
}
