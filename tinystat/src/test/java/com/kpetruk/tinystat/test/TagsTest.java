package com.kpetruk.tinystat.test;

import com.kpetruk.tinystat.Statistics;

public class TagsTest {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    //init
    Statistics s = Statistics.getInstance();

    String c1 = "counter 1";
    String c2 = "counter 2";
    String c3 = "counter 3";
    String c4 = "counter 4";
    String c5 = "counter 5";
    String c6 = "counter 6"; //never incremented
    
    String t1 = "tagOne";
    String t2 = "tagTwo"; 
    String t3 = "tagThree";
    
    //adding tags possible before any counter increments
    s.addCounterTag(t1, c1);
    s.addCounterTag(t1, c2);
    s.addCounterTag(t1, c3);
    
    //fill
    s.increment(c1, 11);
    s.increment(c2, 22);
    s.increment(c3, 33);
    s.increment(c4, 44);
    s.increment(c5, 55);
    //tags added after increments
    s.addCounterTag(t3, c3);
    s.addCounterTag(t3, c5);
    s.addCounterTag(t2, c6);
    //add existent tag (should not be doubled)
    s.addCounterTag(t1, c2);
    
    System.out.println(s.outCounterTag(t1));
    System.out.println(s.outCounterTag(t2));
    //increment c3 in between
    s.increment(c3);
    System.out.println(s.outCounterTag(t3));
  }
  
}
